ODJ Réunion du 3 septembre 2021
----------------------------------

- Introduction
- Point communication bienveillante
- Budget pour se faire aider ?
- Quizz / débat sur le cahier des charges juridique
- Quizz débat sur le cahier des charges architectural
- Point Gouvernance


Plan des réunions
-----------------

Prérequis
---------

Les réunions doivent se tenir en dehors des repas et de tout autre discussion qui n’ont pas de rapports avec le projet si possible sans enfants.  

- Définir GO de réunion : faire le point sur les commissions.  

Déroulement
-----------

Rappel de l’ordre du jour.  

- Ordre du jour type 
- Bilan des commissions 

1°/ Planning
------------

2°/ Gouvernance
---------------

- lecture du CR sur la gouvernance. 
- Délai entre les décisions pour avoir du temps.  
- Apprendre à parler et à s’écouter CNV.  
- Si besoin se faire aider par un extérieur si point de blocage. 

3°/ Formes juridiques
---------------------

réponses aux questions 

- faire le bilan 

4°/ architecture
----------------

réponses aux questions 

- faire le bilan 

5°/ Budget préliminaire
-----------------------

50 euros habicoop ? Oui 

Appeler Pierre ou Yves (Angéline)

- Questions 
- Point d’étape par rapport au rétroplanning 
- Niort 

16-17 octobre 2021
------------------

- train ou voiture ? Voiture et train -location voiture 
- location dans Niort – à chercher rapidement 

planning : une journée dans Niort / 1 visite de maison 

- Définir la date et le lieu de la prochaine réunion : samedi 11/09 après-midi à la coloc/ jeudi 23/09 au soir chez Julie et Stéphane 
- L’ordre du jour : formes juridiques (avoir lu chacun les documents)/ bilan des questions (Benjamin) 
- Rappel des tâches de chacun : lire le document 

