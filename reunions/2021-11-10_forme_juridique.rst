Réunion du 2021-11-10 aux Ulis sur la forme juridique
=====================================================

Présents
--------

- Éric
- Julie
- Stéphane
- Angéline
- Benjamin

Rapporteur
----------

- Stéphane Clérambault

Ordres du jour
--------------

Forme juridique :
  Creuser plus sur la SCI. Il semble que la forme juridique préférable soit la
  *SCI*. Cependant nous aimerions en savoir sur le fonctionnement, notamment
  concernant les loyers et les impôts.

Commission architecture :
  Regarder les dômes géodésiques.

Tout le monde se mets à chercher une maison. Angers et Tour sont plus cher que
Niort. Il faut mettre un léger coup d’accélérateur sur les recherches.


Prochaine réunion
-----------------

Prochaine réunion le vendredi 10 décembre 2021.

Ordre du jour prévisionnel :

- Statuer sur la forme juridique.
- Puis se renseigner au plus vite auprès des banques pour simuler les capacités d’emprunt.
- Lancer les démarches pour la création de l’entreprise.
