11 juillet 2021
+++++++++++++++++++++++

Le lieu Foufou
=================

- Un tiers lieu : co-working, bar, épicerie, ressourcerie
- Potager robotisé
- Repair-café / Tarot
- La grosse chouille 1 fois par an (Event)
- Cabane dans les arbres
- Piscine écologique naturelle

La pièce commune
==================

- Salon genre coloc avec cuisine ouverte
- Atelier Fablab
- Pièce loisir créatifs
- Pièce apéro + jeux société
- Salon avec grand bar en bois

Les commissions
=================

- Processus décisionnel ( comment on décide jusqu'à l'emménagement ) -> Stéphane, Angéline
- Financier ( Structure, capacité d'emprunt ) -> Éric, Julie
- Plan juridique -> Angéline, Benjamin
  - En quel nom à la banque ? Crée on une association ?
  - Structure
- Architecture et terrain (recherche, critères) -> Benjamin, Stéphane
- Rétroplanning, maitre du temps -> Éric, Julie²

Liste des Villes éligibles
=============================

- 4 Niort
- 5 Nantes
- 1 Poitiers
- 3 Tours
- 1 Blois
- 0 Orléans
- 0 Le Mans
- 2.5 Angers
- 1 Angoulème
- 1 Bordeaux

Autres critères à envisager:
----------------------------

- École ~ 7km
- OVH ? @30 minutes
- Collège ?
- Crèche ?
- Fibre
- Arbres

Actions pour la prochaine fois
===============================

- Contacter Myriam (Architecture)
