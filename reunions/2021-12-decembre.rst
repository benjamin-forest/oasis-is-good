Oasis Is Good - 10 décembre - Compte rendu de réunion
=========================================================

+----------------+------------------+
| GO de réunion  | Secrétaire sexy  |
+================+==================+
|    Julie       |  Benjamin        |
+----------------+------------------+


Ordre du jour
-------------

    - Bilan des commissions
    - Timing déménagement
    - SCI / choix définitif
    - Rappel des dates des visites (We des 8 et 29 janvier ?)
    - Questions
    - Brainstorming nom sci
    - Point d’étape par rapport au rétroplanning
    - Définir la date et le lieu de la prochaine réunion
    - L’ordre du jour de la prochaine réunion
    - Rappel des taches de chacun

Préalable
--------------

On remarquera qu'Éric est le seul à venir sans ses enfants aux réunions, comme proposé dans les rêgles de fonctionnement.

Retour Anne Sophie
---------------------

- Jamais eu d'habitat participatif
- SCI ou copro. Dans les deux cas, envisager tout les cas de figures pour préparer la suite.


Copro et sci sont possibles, il est possible d'adapter les statuts de la SCI La SCI est plus engageant que la copro.


Anne Sophie pense que la banque ne sera pas sensible aux statuts, et la solvabilité sera plus importante.
On pourrait commencer à demander les solvabilités individuelles.


Elle Pense qu'avec ~200 000€ d'apport, ont peut peut-être avoir plus de 600 000€.


Attention aux problèmes de santé.


Elle va un peu regarder, mais n'est pas sûre de pouvoir nous suivre sur le long terme. A voir en fonction de notre planning et du sien.


La banque à priori s'en moque qu'on soit en télétravail. Ils vont regarder les CDI, CDD, mais pas forcément le droit de faire du télétravail.


Si Julie n'a pas son concours elle compte essayer de se faire passer en CDI en anticipé. Cela aiderai à la portabilité du poste en déménagement.


Stéphane et Julie doivent décider avant d'aller voir la banque comment ils font avec leur appartement.


En SCI c'est plus compliqué de récupérer l'argent chez les gens que dans une copropriété.


Il faut négocier avec la banque.


Elle va regarder si elle trouve des articles.


Fiscalité : il fallait qu'elle regarde


Timing déménagement
----------------------

Pour info, si Julie a son concours, elle a 6 mois à l'école mais n'a son poste qu'en septembre 2022.
Puis elle commence direct.


Ensuite on va habiter à côté de là ou Julie à son poste, si ça convient à tout le monde.


Si le poste est un poste de gestionnaire de Collège ou Lycée, il faudra voir car il y a un logement de fonction et potentiellement des astreintes.


Résultats le 21 janvier -> à suivre.


Pour Angéline, il n'y a que 7 mois sur 3 ans à Bordeau, du coup si elle l'a cette année on est tranquilles au moins trois ans. Il faudra voir à ce moment là.
Si elle a son concours en juin, oraux septembre octobre, entré à l'école février 2023.


Angéline et Benjamin pour l'instant sont moins pressés, et pourront emménager plus tard dans l'année voire en septembre 2023.


Point sur la SCI. Validation de la SCI comme structure définitive ?
---------------------------------------------------------------------

Julie et Éric accepteraient le rôle de gérant.

Essayer de se laisser la porte ouverte à des locations, etc.

Qui emprunte réellement : la SCI en tant que personne morale ou chacun individuellement sur compte courant ?

Comment on gère les différences de loyer par rapport à la surface habitée ?.


Brainstorming nom SCI
-----------------------

Aux Bons Amis, le bwa du rwa,

Bilan des commissions
-----------------------

Null !

Dates de visites
--------------------

Il y a un tricount à remplir : https://cloud.blasters.fr/index.php/apps/cospend/s/54067f1a13c2faee4dc508e51f1342af


On essaie de faire garder les enfants le We des 29-30

Il restera Le Mans, Orléans (sur la journée ?).

We du 8-9 janvier
++++++++++++++++++

Angers, à partir du vendredi soir, déjà réservé.

We du 29 - 30 janvier
++++++++++++++++++++++++

Rien réservé / à faire
Tours : Samedi et dimanche matin.

Questions
---------------

Éric : pas besoin de demander pour mon attestation télétravail ?
R : non.

Point rétroplanning
---------------------

Pas accessible

Prochaine réunion
-------------------

Date
++++++

We des 8-9 janvier.


Ordre du jour
++++++++++++++++

- SCI : maj
- Point banques
- Point visites

Tâches de chacun
++++++++++++++++++

- Prendre rendez vous avec des banques.

  - Crédit Agricole chevry : **Stéphane** prend rendez vous et y va avec Éric
  - Caisse d'épargne : **Angéline** et Julie
  - Crédit coopératif : **Benjamin** et Éric

- Réserver Tours (sans enfants pour les Clérambault)

  - logement **Benjamin**
  - visite lieu **Benjamin** : on peut voir deux lieux.

- Angers : réserver une visite **Stéphane**. On peut voir deux lieux.
- On se met des alertes sur Orléans.