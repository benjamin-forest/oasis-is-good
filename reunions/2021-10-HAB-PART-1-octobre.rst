Objectif réunion 1er octobre
=============================


- Point rétroplanning et mise à jour
- Compte rendu de la commission financière
- Synthèse de la discussion sur le choix de la forme juridique
- Synthèse de la discussion sur le choix du lieu
- Planning de Niort

GO de réunion :
  Eric

Secrétaire de réunion :
  Julie

Le mode de décision est la décision le consensus. Tout le monde doit adhérer à la proposition. Il faut faire des tours de décision avec des discussions ouvertes et bienveillante.

Remise à jour du retro planning.

Compte rendu commission Financière
----------------------------------

Julie a contacté un courtier et une fille du CA de Loire.

On doit choisir la forme juridique avant de savoir combien emprunter.

Attention à la situation de benji et chouchou car ils devront prouver 3 ans de compte si le statuts sur leur fiche de paye est gérant.
Le statut de gérant n'apparait pas sur les fiches de paies.
Peut être que le fait que la société soit dans l’informatique cela peut passer.

Pour le moment seul le salaire d’Angéline et Eric sont sûrs d'être pris en compte pour le prêt.

Le courtier nous demande de lui fournir :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Carte d’identité
- 2 derniers avis d’imposition revenu
- 3 fiches de salaire
- Échéancier prêt immo, conso, LOA

Accord pour passer par un courtier - Papiers à donner pour mardi 12 octobre - Julie centralise les documents et dit au courtier qu'on chercher un banque le plus éthique possible (crédit coopératif par exemple)

Angéline va contacter sa copine notaire et son oncle.

Bilan des comptes :
~~~~~~~~~~~~~~~~~~~

- Julie et chouchou 100 000€ d’apport
- Benji et Ange 100 000€ d’apport, le salaire d’Angéline compte 2383 €
- Éric pas d’apport mais un salaire qui compte lui. 2700€ net
- 600 000€ d’emprunt sur un simulateur de prêt.


Synthèse de la discussion sur le choix de la forme juridique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Point qui sera traité lors de la prochaine réunion : responsable Angé et Benjaminou


Synthèse de la discussion sur le choix du lieu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Point qui sera traité lors de la prochaine réunion : responsable Benj et Stéphane

Planning de Niort
~~~~~~~~~~~~~~~~~

2 visites dans la journée, grosse visite car bien très gros
date à revoir avec la dame de l'agence

