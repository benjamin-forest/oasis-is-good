.. Oasis Is Good documentation master file, created by
   sphinx-quickstart on Sun Jul 25 15:47:50 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Oasis Is Good's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   raison_detre
   cahier_des_charges
   architecture
   montage_juridique
   regles
   biblio


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
