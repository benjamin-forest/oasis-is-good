Montage juridique
===================

Les montages existants
--------------------------

Synthèse
+++++++++

.. list-table:: Synthèse
   :header-rows: 1

   * -
     - Copropriété (2)
     - SCI (3)
     - Coopérative d'habitants (4)
     - Sociétés d’attribution et d’autopromotion (SAA) ou proche: SCIA (5)
     - Société Coopérative d’Intérêt Collectif qui sera de type SA, SAS ou SARL (6)
     - Association (7)
   * - Forme d'emprunt
     - Individuel (Bien connu des banques)
     - Collectif uniquement
     - ?
     - Individuel
     - ?
     - ?
   * - Gouvernance
     - Fonction des tantièmes
     - Prorata des part sociales acquises (principe 1 personne = 1 voix possible).
     - ?
     - 1 personne = 1 voix possible pour certaines décisions.
     - ?
     - ?
   * - Complexité Création
     - Simple (bien connu des interlocuteurs)
     - Bien connu également
     - ?
     - Plus long à monter que la SCI (division en lots)
     - ?
     - ?
   * - Location
     -
     - L'objet d'une SCI ne peut être commercial
     - ?
     - ?
     - ?
     - ?
   * - Revente
     - Au prix du marché +- histoires de travaux à faire ?
     - Au prix du marché, mais les bénéfices sont reversés au prorata des parts sociales ?
     - Plafonné ? La société détient le bien, nous on détient des part sociales. Il y a une histoire d'Indice de référence des loyers `<https://www.service-public.fr/particuliers/vosdroits/F13723>`_ .
     - Au prix du marché
     - ?
     - Le bénéfice est pour l'association qui n'a a priori pas le droit de le reverser à des particuliers. Donc pas de spéculation possible.
   * - Possibilité de choisir qui entre / sort
     - Impossible
     - Possible
     - ?
     - ?
     - ?
     - ?
   * - Impots ?
     - ?
     - ?
     - ?
     - ?
     - ?
     - ?
   * - Assurance etc.
     - ?
     - ?
     - ?
     - ?
     - ?
     - ?
   * - Aides à la rénovation énergétique
     - ?
     - ?
     - ?
     - ?
     - ?
     - ?
   * - Délai création
     - ?
     - Rapide
     - ?
     - ?
     - ?
     - ?
   * - Complexité usage
     - ?
     - Société -> multiples obligations (différentes options possibles mais il faudra une gérance, ...)
     - ?
     - ?
     - ?
     - ?
   * - Partage espaces communs
     - Lots + espaces communs
     - ?
     - ?
     - Lots
     - ?
     - ?
   * - Accès à la propriété
     - Oui
     - ?
     - ?
     - ?
     - ?
     - ?

Note : pour la rénovation énergétique : contactez l’Espace Info-Energie près de chez vous.

Copropriété
+++++++++++

Un résumé assez clair est disponible dans mon livre sur les clefs de l'habitat participatif.

Le syndicat de copropriété est l'organe qui gère.

- Création : https://www.pap.fr/patrimoine/copropriete/creer-une-copropriete/a5644


SCI
++++

Il existe des variantes
https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_civile_immobili%C3%A8re

Il faut la gérer comme une société, ce qui est contraingnant (Statuts, impots compta, ...)

Chacun prête de l'argent à la société, qui devra la rembourser à un moment ou un autre. Ce mécanisme de prêt se fait via ce qu'on appelle un compte courant d'associé.

Les coopératives d’habitants
+++++++++++++++++++++++++++++

Jouissance de son logement

Les sociétés d’attribution et d’autopromotion
++++++++++++++++++++++++++++++++++++++++++++++

Construction puis jouissance du logement.

https://www.ecologie.gouv.fr/habitat-participatif-cadre-juridique-habiter-autrement

Biblio
-------

- `pdf reprenant une session de travail d'un groupe <https://www.leshabiles.org/wp-content/uploads/2018/07/2016_02_1-le-juridique-dun-projet-HP.pdf>`_


Plan de réunion 1 choix structure juridique
------------------------------------------------

Objectif : composer un cahier des charges juridique de la structure.


Plan de réunion 2 choix structure juridique
------------------------------------------------

Création d'une association ?

Plan de réunion 3 choix structure juridique
------------------------------------------------

Présentation des possibilités à la lumière des réunions précédentes.