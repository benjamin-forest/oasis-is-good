Architecture
=============

Assistance à maitrise d'ouvrage
---------------------------------

Doit on se faire aider ? Par qui ?


Plan Local d'urbanise et autre lois
------------------------------------

Le checkup healthy avant de signer.

PLU : on peut demander à Florian ce n'est pas très compliqué.

Projet Cendrillon
--------------------

On récupère un chateau ou un manoir. Il y a une grande bâtisse, et de petites dépendances ...

Comment on gère ? Division de la grande bâtisse en maison mitoyenne ?
Maison d'hôte ?

Projet Gilles de Ham
-----------------------

On récupère une ferme avec suffisement de dépendances pour tout le monde.


Questions
-------------

- Envisage-t-on de vivre dans la maison contenant les pièces partagées ?
- Envisage-t-on de construire les pièces partagées
- Envisage-t-on de construire une ou des maisons neuves
- Envisage-t-on de faire des travaux importants (isolation, fenêtres, chauffage) dans le bien acheté
- Envisage-t-on de mutualiser l'eau et l'électricité ?
- Envisage-t-on d mutualiser le chauffage ?
- Souhaite-t-on avoir un espace de jardin privatif
- Envisage-t-on une maison mitoyenne ?
- Envisage-t-on de transformer un bâtiment en maison mitoyenne ?
- Envisage-t-on de confier tout ou partie de la maitrise d'ouvrage et des plans à un architecte ?
- Si il y a trop de surface, qu'envisage-t-on d'en faire ? Chambre d'hôte ?
- Distance entre les maisons ?
- Envisage-t-on d'acheter un terrain nu ?
- Taille terrain ?
- Installe-t-on des panneaux solaires ?
- Veut on une maison lumineuse

Exposition, agencement des maisons.


Faire la liste des critères à vérifier pour maison healthy.