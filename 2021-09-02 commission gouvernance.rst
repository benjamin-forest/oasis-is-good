" Un réel pouvoir repose non pas sur la capacité de contrôler mais sur celle de faire confiance"

unanimité / autoritaire / vote à la majorité / compromis / consentement / consensus / hasard / jugement majoritaire / décision par classement de préférence 

attention au leadership : chacun doit trouver sa place

Prise de décision par consentement : pilier de la gouvernance sociocratique (évite le vote et les frustrations, le consensus et ainsi l'accord de tous, ou encore le "pour ou contre")
consentement : accepte sans avoir quelque chose à redire. Ou alors objection argumentée

utiliser la communication non violente. Peut-être voir si on se prépare un atelier pour se familiariser ? 
Faire des pauses au moment des objections.

Pour chaque réunion: désigner un animateur, un scribe, un gardien du temps et un donneur de parole.
Prendre 10 min au début des réunions pour partager sur notre humeur. Il faut veiller à instaurer un tour de parole. 

Interdiction de critiquer sans proposition constructive

Le vote est rarement utilisé dans les habitats participatifs, ils préfèrent le consensus. 

Faire des tours de table silencieux avant de commencer à débattre, pour éviter les influences. 
