Cahier des charges
===================

Liste des Villes éligibles
=============================

- 4 Niort
- 5 Nantes
- 1 Poitiers
- 3 Tours
- 1 Blois
- 0 Orléans
- 0 Le Mans
- 2.5 Angers
- 1 Angoulème
- 1 Bordeaux

Autres critères à envisager:
----------------------------

- École ~ 7km
- OVH ? @30 minutes
- Collège ?
- Crèche ?
- Fibre
- Arbres
